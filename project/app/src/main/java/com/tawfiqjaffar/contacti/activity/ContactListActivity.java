package com.tawfiqjaffar.contacti.activity;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawfiqjaffar.contacti.R;
import com.tawfiqjaffar.contacti.adapters.ContactListRecyclerAdapter;
import com.tawfiqjaffar.contacti.mock.ContactList;

public class ContactListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;


    private String userName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        this.userName = getIntent().getStringExtra("NAME_KEY");
        Log.d("CONTACT_LIST_ACTIVITY", "Hello");
        this.setTitle(this.userName);
        setupViews();
    }

    private void setupViews() {

        ContactListRecyclerAdapter adapter = new ContactListRecyclerAdapter(this, ContactList.contactList);

        this.recyclerView = findViewById(R.id.recycler);

        this.recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        this.recyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();


    }
}