package com.tawfiqjaffar.contacti.presenter;

import com.tawfiqjaffar.contacti.listener.MainListener;

public class MainPresenter {


    private MainListener listener;

    public MainPresenter(MainListener listener) {
        this.listener = listener;
    }

    public void didClickOnSubmitButton(final String name) {
        final String text = "Welcome " + name + ", to continue, click ok";

        listener.showDialogWithText(text);
    }


}
