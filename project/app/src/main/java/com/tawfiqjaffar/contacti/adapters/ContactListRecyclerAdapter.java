package com.tawfiqjaffar.contacti.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawfiqjaffar.contacti.R;
import com.tawfiqjaffar.contacti.model.Contact;

import java.util.List;

public class ContactListRecyclerAdapter extends RecyclerView.Adapter<ContactListRecyclerAdapter.ContactItemViewHolder> {

    private List<Contact> contactList;
    private Context context;

    public ContactListRecyclerAdapter(Context context, List<Contact> contactList) {
        this.contactList = contactList;
        this.context = context;

    }

    @NonNull
    @Override
    public ContactItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.contact_item, parent, false);
        return new ContactItemViewHolder(contactView);
    }


    @Override
    public void onBindViewHolder(@NonNull ContactItemViewHolder holder, int position) {
        Contact currentItem = this.contactList.get(position);

        holder.name.setText(currentItem.getFirstName() + " " + currentItem.getLastName());
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    class ContactItemViewHolder extends RecyclerView.ViewHolder {


        public TextView name;

        public ContactItemViewHolder(@NonNull View itemView) {
            super(itemView);

            this.name = itemView.findViewById(R.id.contact_name);
        }
    }


}
