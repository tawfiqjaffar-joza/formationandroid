package com.tawfiqjaffar.contacti.mock;

import com.tawfiqjaffar.contacti.model.Contact;

import java.util.Arrays;
import java.util.List;

public class ContactList {
    public static List<Contact> contactList = Arrays.asList(
            new Contact("Nassiloche", "LaReineDesBrioches", "0102030405", "nassiloche@email.com"),
            new Contact("Antoine", "Leon", "012345567", "antoinedu93@email.com"),
            new Contact("Hamza", "Hassen", "0102030405", "hamzahassen@email.com"),
            new Contact("Philippe", "Roudaut", "0123124543", "phiphity92@live.fr"),
            new Contact("Yassine", "Rachat", "01234234", "yassine@magny.com"),
            new Contact("Berk", "Oneronair", "0123124512", "onair@email.com"),
            new Contact("Yani", "Mira", "1231231231", "po@email.com")
    );
}
