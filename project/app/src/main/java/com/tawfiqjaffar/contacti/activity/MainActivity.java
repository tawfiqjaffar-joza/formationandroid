package com.tawfiqjaffar.contacti.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.tawfiqjaffar.contacti.R;
import com.tawfiqjaffar.contacti.listener.MainListener;
import com.tawfiqjaffar.contacti.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements MainListener {

    private Button submitButton;

    private EditText nameField;

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        this.presenter = new MainPresenter(this);
        setupViews();
    }

    private void setupViews() {
        this.submitButton = findViewById(R.id.button);
        this.nameField = findViewById(R.id.name_field);

        this.submitButton.setOnClickListener(view -> {
            presenter.didClickOnSubmitButton(nameField.getText().toString());
        });
    }

    @Override
    public void showDialogWithText(String text) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Welcome")
                .setMessage(text)
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    dialogInterface.dismiss();

                    Intent intent = new Intent(this, ContactListActivity.class);

                    intent.putExtra("NAME_KEY", nameField.getText().toString());

                    startActivity(intent);

                }).create();
        dialog.show();
    }
}